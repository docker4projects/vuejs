# Docker for VueJs

<p style="text-align:center">
    <img width="100" src="https://vuejs.org/images/logo.png" alt="Vue logo">
</p>

> This project is based on [VueJs CLI](https://cli.vuejs.org/).

## Creating a new project

> If you have already created a project, go to [the next step](#launching-vuejs-application)

Use the Docker file generator to build a new project.

```bash
$ docker build -f Dockerfile.generator -t vuejs .
```

> If you use **Windows**, a `WARNING` can appear at the end of install. Don't forget to check and reset permissions for sensitive files and directories.

To run the container in your main directory:

```bash
// Linux
$ docker run -itd -v ${PWD}:/app --name vuejsWeb vuejs

// Windows
$ docker run -itd -v %cd%:/app --name vuejsWeb vuejs
```

Now you can use `vue` command without Vue CLI in your machine.  
You can create a new VueJs project with this next command.

```bash
$ docker exec -it vuejsWeb vue create project-name
```

You can remove `vuejsWeb` container as you want.

```bash
$ docker rm -f vuejsWeb
```

And you can drop `Dockerfile.generator`.

## Launching VueJs application

Now, you just need move the `Dockerfile`, `.dockerignore` and the `docker-compose.yml` in your root project. Run command to run application server. Navigate to [`http://localhost:8080/`](http://localhost:8080/).

```bash
$ docker-compose up
```

## Production

You just need move the `Dockerfile.prod`, `.dockerignore`, `docker-compose.prod.yml` and `nginx.conf` in your root project. Run command to run production server. Navigate to [`http://localhost:8080/`](http://localhost:8080/).

```bash
$ docker-compose -f docker-compose.prod.yml up --build
```
